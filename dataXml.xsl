<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
  <div class="card-columns">
    <xsl:for-each select="rss/channel/item[position() &lt; 7]">
      <div class="card">
        <img src="{enclosure/@url}" class="card-img-top"/>
        <div class="card-body">
          <p class="card-text"><xsl:value-of select="title/../../title" /></p>
          <h5 class="card-title"><xsl:value-of select="title" /></h5>
          <p class="card-text"><small class="text-muted"><xsl:value-of select="pubDate" /></small></p>
          <a href="{link}" class="btn btn-primary btn-lg btn-block">Open Link</a>
        </div>
      </div>
    </xsl:for-each>
  </div>
</xsl:template>
</xsl:stylesheet>







