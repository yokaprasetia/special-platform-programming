<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>XML WEBSITE</title>

    <style>
        body {
            background-color: gray;
        }

        img {
            width: 300px;
        }
    </style>

    <script>
        function loadXMLDoc(filename) {
            if (window.ActiveXObject) {
                xhttp = new ActiveXObject("Msxml2.XMLHTTP");
            } else {
                xhttp = new XMLHttpRequest();
            }
            xhttp.open("GET", filename, false);
            try {
                xhttp.responseType = "msxml-document"
            } catch (err) {} // Helping IE11
            xhttp.send("");
            return xhttp.responseXML;
        }

        function displayResult() {
            $source = ['https://www.antaranews.com/rss/hukum.xml', 'https://www.antaranews.com/rss/sepakbola-liga-indonesia.xml', 'https://www.antaranews.com/rss/hiburan-musik.xml'];


            for ($i = 0; $i < 3; $i++) {
                xml = loadXMLDoc($source[$i]);
                xsl = loadXMLDoc("dataXml.xsl");
                // code for IE
                if (window.ActiveXObject || xhttp.responseType == "msxml-document") {
                    ex = xml.transformNode(xsl);
                    document.getElementById("example").innerHTML = ex;
                }
                // code for Chrome, Firefox, Opera, etc.
                else if (document.implementation && document.implementation.createDocument) {
                    xsltProcessor = new XSLTProcessor(xml);
                    xsltProcessor.importStylesheet(xsl);
                    resultDocument = xsltProcessor.transformToFragment(xml, document);
                    document.getElementById("example").appendChild(resultDocument);
                }

            }
        }
    </script>
</head>

<body id="example" onload="displayResult()" class="p-3">
    <h2 class="text-center mt-4">DAFTAR BERITA SEPUTAR INDONESIA</h2>
    <p class="text-center mb-5">This website is made by XML</p>

    <div id="example"></div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>