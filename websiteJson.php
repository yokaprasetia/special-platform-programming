<?php
$source = ['https://www.antaranews.com/rss/hukum.xml', 'https://www.antaranews.com/rss/sepakbola-liga-indonesia.xml', 'https://www.antaranews.com/rss/hiburan-musik.xml'];
for ($i = 0; $i < 3; $i++) {
    $xml = simplexml_load_file($source[$i]);
    $json = json_encode($xml, JSON_PRETTY_PRINT);
    $data[$i] = json_decode($json, true);
}
?>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

    <title>JSON WEBSITE</title>

    <style>
        body {
            background-color: gray;
        }
    </style>
</head>

<body>
    <h2 class="text-center mt-4">DAFTAR BERITA SEPUTAR INDONESIA</h2>
    <p class="text-center mb-5">This website is made by JSON</p>
    <div class="row row-cols-1 row-cols-md-3">
        <?php
        foreach ($data as $result1) :
            foreach ($result1 as $result2) :
                foreach ($result2 as $item) :
                    if (is_array($item)) {
                        for ($i = 0; $i < 6; $i++) {
        ?>
                            <div class="col">
                                <div class="card m-1">
                                    <img src="<?php echo $item[$i]['enclosure']['@attributes']['url']; ?>" class="card-img-top">
                                    <div class="card-body">
                                        <h5 class="card-title"><?php echo $item[$i]['title'] ?></h5>
                                        <p class="card-text"><small class="text-muted"><?php echo $item[$i]['pubDate'] ?></small></p>
                                        <a href="<?php echo $item[$i]['link'] ?>" class="btn btn-primary btn-lg btn-block">Kunjungi Situs</a>
                                    </div>
                                </div>
                            </div>
        <?php
                        }
                    }
                endforeach;
            endforeach;
        endforeach;
        ?>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
    -->
</body>

</html>